var http = require('http');
var https = require('https');
var fs = require('fs');

var Monitor = function() {};

function ping(severs, actual, callback) {

	if (actual < 0)
		return callback(severs);

	var element = severs[actual];

	var schema = getSchema(element.url);

	schema.get(element.url, (res) => {
		if (res.statusCode == 200 || res.statusCode == 301 || res.statusCode == 302)
			element.status = 'on';
		else
			element.status = 'off';

		return ping(severs, actual - 1, callback);

	}).on('error', (e) => {
		console.log(`Got error: ${e.message}`);
		severs.splice(actual, 1);
		return ping(severs, actual - 1, callback);
	});
}

function getSchema(url) {
	var schema = '';
	if (isHttps(url))
		schema = https;
	else
		schema = http;
	
	return schema;
}

function isHttps(url) {
	if (url.startsWith("https://"))
		return true;
	return false;
}

Monitor.prototype.check = function(severs, callback) {
	return ping(severs, severs.length - 1, callback);

};

module.exports = new Monitor();
